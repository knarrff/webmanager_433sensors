# webmanager_433sensors

This small collection of cgi scripts let users manage a yaml file containing sensors detected by [rtl_433](https://github.com/merbanan/rtl_433).

Some of these sensors, like the TFA-303221 I use, change their ID every time the batteries are changed, but I wanted to assign names to specific sensors past their battery life time, e.g., to pass on that information to a smart home management system.
Because of this, I needed something that resolves the combination <pre>ID_CHANNEL</pre> to a name I can specify, and a way to easily update that mapping every time batteries are changed.
I like simplicity, so I store this information in a plain text file.
However, since I also have quite a few of those sensors and less technically inclined persons should be able to change batteries and re-integrate the sensers into the system, I needed something that did not involve changing a text file on some server: a web page everyone can use even from their phone.
This is it: two scripts that are used to display and edit the information in that text file, as well as a workflow to detect new or changed sensors (after battery replacement).

You can either integrate the cgi scripts in a proper web server or test them using

<pre>
  RTL433_BIN=PATH_RTL433/rtl_433 python3 -m http.server --cgi
</pre>
where <pre>PATH_RTL433</pre> is the path to the executable <pre>rtl_433</pre> from the project [rtl_433](https://github.com/merbanan/rtl_433). Consult your web server documentation to see how to pass this required environment variable there.

Since this will be used in Germany, all of the user-facing text is currently only written in German. It would not take me long to translate it, but I currently see no use for it. However, I am willing to integrate pull-requests that add language options, and I might even be willing to do that myself if someone asks nicely.

