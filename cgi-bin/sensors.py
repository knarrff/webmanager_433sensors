#!/usr/bin/env python3
import cgi, sys
import html

# DISABLE these in production environments!
import cgitb
cgitb.enable()

import yaml

print("Content-Type: text/html\n")

def html_head():
    print("""<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
</head>
""");

# load sensor data
sensor_db = {}
try:
    sensor_db_fd = open("sensors.yaml")
    sensor_db = yaml.safe_load(sensor_db_fd)
    sensor_db_fd.close()
except:
    pass

form = cgi.FieldStorage()

# return id and channel of a sensor for a given valid primary key (pkey), otherwise return False
def valid_sensor_id(sensor_pkey):
    parts = sensor_pkey.split('_', maxsplit=1)
    if len(parts) == 2 and parts[0].isdigit() and parts[1].isdigit():
        (sensor_id, sensor_channel) = map(lambda x: int(x), parts)
        if sensor_id < 256 and sensor_channel > 0 and sensor_channel < 4:
            return (sensor_id, sensor_channel)
    return False

# check if we got sensor information to add od edit
if form.getfirst('add') != None or form.getfirst('edit') != None:
    if form.getfirst('add') != None:
        sensor_pkey = form.getfirst('add')
    else:
        sensor_pkey = form.getfirst('edit')
    sensor_name = form.getfirst('name')
    # sanity checks on the passed data
    if valid_sensor_id(sensor_pkey):
        (sensor_id, sensor_channel) = valid_sensor_id(sensor_pkey)
        if (len(sensor_name) > 0 and len(sensor_name) < 50 and
            not any([c in sensor_name for c in set('\n\r<>"\'')])):
            # This looks like valid sensor data
            # Check if a sensor with this name already exists (in case the request was to add a new one instead of editing an existing one)
            if form.getfirst('edit') == None and sensor_name in map(lambda x: x['name'], sensor_db.values()):
                print(f"<p>Ein Sensor mit dem Namen \"{sensor_name}\" ist schon bekannt. Aktualisiere seine Daten über den entsprechenden Knopf (nicht über anlernen) oder entferne ihn vorher.</p>")
            # Check if a sensor with the same ID on the same channel is already known, but has a different name
            elif sensor_pkey in sensor_db and sensor_db[sensor_pkey]['name'] != sensor_name:
                print(f"<p>Ein Sensor mit ID ({sensor_id}) ist auf Kanal ({sensor_channel}) schon bekannt unter dem Namen \"{sensor_db[sensor_pkey]['name']}\". Dadurch, dass die ID beim Einlegen der Batterien vom Sensor zufällig erzeugt wird und es nur 256 Möglichkeiten dafür gibt, kann es wie hier durchaus einmal vorkommen, dass man dabei zufällig die ID eines anderen Sensors erwischt.")
                # Try to see if the same ID is available on a different channel, and if so: suggest this as one of the ways to continue
                free = list(filter(lambda x: x not in sensor_db,
                                   [f"{sensor_id}_1", f"{sensor_id}_2", f"{sensor_id}_3"]))
                if len(free):
                    print(f" Stelle den Kanal auf {free[0][-1]} (Schiebeschalter oben rechts unter der Batterieabdeckung) oder entferne")
                else:
                    print(" Entferne")
                print(" kurz die Batterien und lege sie wieder neu ein, um eine neue ID generieren zu lassen. Danach kann neu angelernt werden.</p>")
            # If we got here, we passed all checks
            else:
                # make sure we do not have duplicate names, .e.g., because of edits of existing sensors, but also in general to be sure
                for existing in dict(filter(lambda x: x[1]['name'] == sensor_name, sensor_db.items())):
                    del sensor_db[existing]
                # add the new sensor
                sensor_db[sensor_pkey] = {
                    'name'   : sensor_name,
                    'id'     : sensor_id,
                    'channel': sensor_channel,
                }
                # save
                with open('sensors.yaml', 'w') as sensor_db_fd:
                    yaml.dump(sensor_db, sensor_db_fd, default_flow_style=False)
                #print(f"<p>Der Sensor mit dem Namen '{sensor_name}' ist nun unter ID ({sensor_id}) auf Kanal ({sensor_channel}) registriert.</p>")

if form.getfirst('del') != None:
    # sanity checks on the passed data
    sensor_pkey = form.getfirst('del')
    if valid_sensor_id(sensor_pkey):
        (sensor_id, sensor_channel) = valid_sensor_id(sensor_pkey)
        if sensor_pkey in sensor_db:
            #print(f"<p>Der Sensor \"{sensor_db[sensor_pkey]['name']}\" mit ID ({sensor_id}) auf Kanal ({sensor_channel}) wurde entfernt.</p>")
            del sensor_db[sensor_pkey]
            with open('sensors.yaml', 'w') as sensor_db_fd:
                yaml.dump(sensor_db, sensor_db_fd, default_flow_style=False)

print("<html>")
html_head()
print("<body>")

if len(sensor_db) == 0:
    print("Es sind momentan keine Sensoren bekannt.")
else:
    print(" <table>")
    print("  <tr><th>sensor id</th><th>name</th><th></th></tr>")
    for sensor_id, sensor_data in sorted(sensor_db.items(), key=lambda x: x[1]['name']):
        print(f"  <tr>\n   <td>{sensor_id}</td><td>{html.escape(sensor_db[sensor_id]['name'])}</td><td>")
        print(f'   <form action="/cgi-bin/listen_for_sensors.py"><input type="hidden" name="edit" value="{html.escape(sensor_db[sensor_id]["name"])}"><input type="submit" value="Aktualisieren"></form>')
        print(f'   <form action="/cgi-bin/sensors.py"><input type="hidden" name="del" value="{sensor_id}"><input type="submit" value="Entfernen"></form>')
        print("   </td></tr>")
    print(" </table>")

print("""
 <form action="/cgi-bin/listen_for_sensors.py">
  <input type="submit" value="neuen Sensor anlernen">
 </form>
""")

print("</body>\n</html>")
