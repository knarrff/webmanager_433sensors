#!/usr/bin/env python3
import sys, os
import cgi

# DISABLE these in production environments!
import cgitb
cgitb.enable()

import subprocess
import yaml
import json

def html_head():
    print("""<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
</head>
""");

if 'RTL433_BIN' not in os.environ or not os.access(os.environ['RTL433_BIN'], os.X_OK):
    print("Content-Type: text/plain\n")
    print("RTL433_BIN not configured, aborting. It should point to the executable rtl_433 from an installation of https://github.com/merbanan/rtl_433.")
    sys.exit(0)
RTL433_BIN = os.environ['RTL433_BIN']

print("Content-Type: text/html\n")

form = cgi.FieldStorage()

name = None
if form.getfirst('edit'):
    name = form.getfirst('edit')

sensor_db = {}
try:
    sensor_db_fd = open("sensors.yaml")
    sensor_db = yaml.safe_load(sensor_db_fd)
except:
    pass

def parse_line(line):
    # do not trust this input
    if len(line) > 1000:
        return None
    try:
        decoded = json.loads(line)
    except:
        return None
    if type(decoded) is not dict:
        return None
    if 'model' not in decoded:
        return None
    if  decoded['model'] != 'LaCrosse-TX141THBv2':
        return None
    if 'id'    not in decoded:
        return None
    if type(decoded['id']) != int:
        return None
    if decoded['id'] < 0 or decoded['id'] > 254:
        return None
    if 'channel' not in decoded:
        return None
    if type(decoded['channel']) is not int:
        return None
    if decoded['channel'] < 0 or decoded['channel'] > 2:
        return None
    if 'test'  not in decoded:
        return None
    if decoded['test'] != 'Yes':
        return None
    return (decoded['id'], decoded['channel']+1)

print("<html>")
html_head()
print("<body>")

print("""
<ul>
 <li>Batteriefach des anzulernenden Sensors öffnen</li>
 <li>ggf. Batterien einlegen</li>
 <li>kleinen, weißen Taster links oben im Batteriefach drücken; die LED sollte daraufhin kurz aufleuchten.</li>
 <li>Kurz danach sollte hier eine entsprechende Meldung auftauchen, ggf. mit der Möglichkeit, den Sensor anzulernen.</li>
</ul>
<p>Ich lausche ...</p>
""")
# The following is necessary for progressive HTML rendering to work
# It fills potentially existing transfer/receive buffers in either a server/proxy or the
# browser to make it display the page until here while still waiting for more.
# Without the next line, this would not happen on Firefox (but would work in Chrome).
# Note that compression might pose a problem, waiting for a buffer to fill.
sys.stdout.flush()

sensor_pkey = None

countdown = 30
process = subprocess.Popen(["timeout", str(countdown+5), RTL433_BIN, "-F", "json"],
                           stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
os.set_blocking(process.stdout.fileno(), False)

sensor_pkey = None
while countdown > 0:
    countdown -= 1
    try:
        process.wait(timeout=1)
    except subprocess.TimeoutExpired:
        for stdout_line in iter(process.stdout.readline, ""):
            if len(stdout_line) == 0:
                break
            if parse_line(stdout_line) != None:
                (sensor_id, sensor_channel) = parse_line(stdout_line)
                sensor_pkey = f"{sensor_id}_{sensor_channel}"
                if sensor_pkey in sensor_db and (name == None or sensor_db[sensor_pkey]['name'] != name):
                    print(f"<p>Diese Kombination aus ID und Kanal ist schon durch den Sensor mit Namen \"{sensor_db[sensor_pkey]['name']}\" belegt. Entweder muss dieser in der Liste gelöscht oder aktualisiert werden, oder alternativ lässt Du den Sensor eine neue ID generieren, indem Du die Batterien kurz entfernst und wieder einlegst oder mit dem Schieberegler oben rechts im Batteriefach einen anderen Kanal probierst. ")
                    free = list(filter(lambda x: x not in sensor_db,
                                       [f"{sensor_id}_1", f"{sensor_id}_2", f"{sensor_id}_3"]))
                    if len(free):
                        print(f"Kanal {free[0][-1]} ist zum Beispiel im Moment noch nicht belegt.")
                    else:
                        print("Allerdings sind alle anderen auch schon belegt.")
                    print("</p>")
                    sys.stdout.flush()
                else:
                    countdown = 0
process.terminate()

if sensor_pkey != None:
    print( '<form action="/cgi-bin/sensors.py">')
    print(' <input type="hidden" name="'+('edit' if name else 'add')+f'" value="{sensor_pkey}">')
    forbidden = '<>\''
    print(f' Namen eingeben (Zeichen <verb>'+forbidden+'"</verb> nicht erlaubt): <input type="text"   name="name" autofocus required minlength="1" maxlength="30" size="30" pattern="[^\\n\\r\\x22'+forbidden+']+"'+(' value="'+name+'"' if name else '')+'>')
    print(f' <input type="submit" value="Sensor '+('aktualisieren' if name else 'anlernen')+'">')
    print( "</form>")
else:
    print('Nicht gefunden, <a href="/cgi-bin/listen_for_sensors.py">neu versuchen</a>.')
print( "</body>\n</html>")

